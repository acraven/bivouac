﻿namespace Bivouac.Model
{
   public enum Availability
   {
      Unknown,
      Available,
      Unavailable,
      Limited
   }
}