namespace Bivouac.Abstractions
{
   public interface IGetRequestId
   {
      string Get();
   }
}